#!/usr/bin/env python3 


import re
import sys

class clr:
    green     = '\033[92m'
    yellow    = '\033[93m'
    red       = '\033[91m'
    blue      = '\033[94m'
    cyan      = '\033[96m'
    magenta   = '\033[95m'
    grey      = '\033[90m'
    rest      = '\033[0m'
    BOLD      = '\033[1m'
    UNDERLINE = '\033[4m'

#print(f"{clr.green} Hellllloooow  {clr.rest}")
#print(clr.red + "Hellloowwww" + clr.rest)


def multiinput1():
    try:
        while True:
            a = input()
            if not a:
            #if a == "":
                break
            yield a
    except KeyboardInterrupt:
        return

def multiinput2():
    while True:
        a = input()
        if not a:
        #if a == "":
            break
        yield a


def calc(incom):
    count = 0
    x = 0.0
    

    check_digits = 0
    for i in incom:
        if re.findall("[^\W\d_]", i):
            check_digits = 1

    if check_digits == 0:
        for i in incom:
            x = x + float(i.lower())
        print(x)
        sys.exit()

    for i in incom:
        
        count += 1 

        MBTS = ['м', 'm' ]
        GBTS = ['г', 'g' ]
        KBTS = ['к', 'k' ]
        BTS  = ['б', 'b' ]

        for GBT in GBTS:
            if re.findall(GBT,i.lower()):
                mstr = float(re.findall("\d+",i)[0])
                print(f'гигабайты. all string: {clr.green}{i}{clr.rest}. cost = {clr.yellow}{mstr}{clr.rest}.' )
                x = x + (mstr * 1024 * 1024)

        for MBT in MBTS:
            if re.findall(MBT,i.lower()):
                mstr = float(re.findall("\d+",i)[0])
                print(f'мегабайты. all string: {clr.green}{i}{clr.rest}. cost = {clr.yellow}{mstr}{clr.rest}.' )
                x = x + (mstr * 1024)

        for KBT in KBTS:
            if re.findall(KBT,i.lower()):
                mstr = float(re.findall("\d+",i)[0])
                print(f'килобайты. all string: {clr.green}{i}{clr.rest}. cost = {clr.yellow}{mstr}{clr.rest}.' )
                x = x + (mstr)

    gb = x / 1024 / 1024 
    mb = x / 1024 
    return gb, mb, count


a = list(multiinput1())
gb, mb, count = calc(a)
print(f'{gb} Gb')
print(f'{mb} Mb')
print(f'{count} strings')


